[![pipeline status](https://gitlab.com/thepiercingarrow/microbot/badges/master/pipeline.svg)](https://gitlab.com/thepiercingarrow/microbot/commits/master)

# Microbot

<https://microbot.iamtpa.xyz/>

Microbot is a finite state machine turtle programming game. You program the turtle, by mapping states to actions, to "sweep" the entire empty space starting from any spot, given a map. In other words, "solve" a map by programming the microbot such that it will touch all empty space on the map starting from any (empty) spot.

## Instructions

The state of a bot is its surroundings and an integer "state" (i.e., whether the tiles to the north/east/west/south are walls or empty space, and an integer representing "state").

Instructions, at most 1 per line, should be formatted `a b -> c d`.  
`a` should be the "state" required for the bot to follow that instruction.  
`b` should be a 4 character expression representing the required surroundings for that instruction to active. The first character should be `N` to represent a wall to the north, `*` to represent a wildcard (it matches both wall/empty), or any other character (excluding whitespace) to represent empty space. The second should be `E` to represent a wall to the east, third `W` for west, and fourth `S` for south.  
`c` should be the direction the bot moves, and `d` should be the new "state" the bot switches to.
No two rules should cover the same state/wall case.

The bot starts at "state" 0.

E.g.  
`0 **** -> E 0` will cause the bot to [move east] until it "crashes" into a wall.  
`0 NEW_ -> S 1` will cause the bot to [move south and switch to "state" 1] if [there are walls to the north/east/south].

# Contact

Feel free to contact me with any questions, suggestions, comments, or just to say hi!

My email is (probably) `me@iamtpa.xyz`. You can also try `thepiercingarrow@gmail.com` and `thepiercingarrow@protonmail.com`. My discord account changes a lot, but I'll (probably) be in this discord server: <https://discord.gg/bvWkwJE>.
